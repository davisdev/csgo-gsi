const http = require('http')
const io = require('socket.io')(http)

// Primarily for checking time for functions
// to run and data output depending on state.
let debug = true

let observable = [
    'isDefusing',
    'isBombPlanted',
    'isTimeout',
    'isLive'
]

http.createServer((req, res) => {
    let serverResponse = {}

    req.on('data', data => {
        serverResponse = JSON.parse(data)
        debug ? console.log('Data incoming: \n', serverResponse) : null
        io.emit('data', serverResponse)

        debug ? console.time('runObservables') : null
        observerEvents(observable)
        debug ? console.timeEnd('runObservables') : null
    })
})

/**
 * Run the event listeners from the
 * array that defines what events can
 * be listened to.
 */
function observerEvents(eventList) {
    if (typeof eventList === 'undefined') {
        throw new Error('List of events to listen to is undefined! Be sure to add desired events to the list.')
    }

    eventList.forEach(event => {
        if (typeof event === 'function') {
            event()

            return
        }

        continue
    })
}

// TODO: Implement which data should be checked for these events.
class EventObserver {
    static isLive() {}
    static isDefusing() {}
    static isTimeout() {}
    static isLive() {}
}

/**
 * Compare if the previous iteration of server sent data is outdated
 * by the new iteration of data (meaning, new/changed properties/values)
 * and returns the resulting object which has the new properties and values.
 * 
 * @param {Object} oldObject Previous iteration of received data from server.
 * @param {Object} newObject New iteration of received data from server.
 */
function difference(oldObject, newObject) {
    let result = {}

    for (property in newObject) {
        if (oldObject[property] === 'undefined' || oldObject[property] != newObject[property]) {
            result[property] = newObject[property]
        }
    }

    return result
}